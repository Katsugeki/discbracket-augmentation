"""
augment.py

Takes a discbracket corpus and returns an augmented corpus based on substructure substitutions.
"""

"""
TODO:
- change N to percentage -> DONE
- disallow insertion of discontinuous constituencies -> DONE
- allow NP-SUJ change with NP-OBJ -> HALF DONE
- produce only discontinuous trees -> DONE
"""

import argparse
from random import randint

from discotree import *

def count_discontinous(tree_list):
    res = 0
    for tree in tree_list:
        if tree.has_discontinuity():
            res += 1
    return res

def produce_trees():
    i = 0
    while i < total:
        print("Phrase ({}).".format(i+1))

        # randomly choose a tree and a constituency to substitute
        tree = choice(tree_list)
        tree_pos = tree.get_random_label_position(label, True)
        while tree_pos == None or tree[tree_pos].is_discontinuous():
            tree = choice(tree_list)
            tree_pos = tree.get_random_label_position(label, True)
        # clone the chosen tree
        newtree = tree.clone()
        print(tree)
        print(tree_pos)
        print(newtree[tree_pos])

        # randomly choose tree and constituency that will be subbed in to newtree
        sub = choice(tree_list)
        sub_pos = sub.get_random_label_position(label, True)
        while sub_pos == None or sub[sub_pos].is_discontinuous() or (args.no_singles and len(sub[sub_pos]) <= 1):
            sub = choice(tree_list)
            sub_pos = sub.get_random_label_position(label, True)
        print(sub)
        print(sub_pos)
        print(sub[sub_pos])
        
        # substitution
        newtree.sub_with_check(tree_pos, sub[sub_pos])
        
        # add newtree to list if does not exist already
        if not newtree in tree_set:
            # if (i+1)%10 == 0 or i+1 == args.n:
            #     print("Generated new phrase ({}).".format(i+1))
            print(newtree)
            if args.use_generated_trees:
                tree_list.append(newtree)
                if newtree.has_discontinuity():
                    disco_tree_list.append(newtree)
            tree_set.add(newtree)
            aug_tree_list.append(newtree)
            newstr = str(newtree)+"\n"
            str_list.append(newstr)
            i += 1

        print()

def produce_disco_trees():
    i = 0
    while i < total:
        print("Phrase ({}).".format(i+1))
        # generate random number between 0 to 99
        flip = randint(0,99)
        # based on percentage of discontinuous trees in original corpus
        # calculate a probability threshold
        threshold = round(len(disco_tree_list)/len(tree_list)*100) if args.allow_disco_sub else 100
        # randomly choose a tree and a constituency to substitute
        tree = choice(tree_list) if flip < threshold else choice(disco_tree_list)
        tree_pos = tree.get_random_label_position(label, True)
        while tree_pos == None or tree[tree_pos].is_discontinuous():
            flip = randint(0,99)
            tree = choice(tree_list) if flip < threshold else choice(disco_tree_list)
            tree_pos = tree.get_random_label_position(label, True)
        newtree = tree.clone()
        print(tree)
        print(tree_pos)
        print(newtree[tree_pos])

        if newtree.has_discontinuity():
            # print("original tree is discontinuous")
            sub = choice(tree_list)
            sub_pos = sub.get_random_label_position(label, True)
            while sub_pos == None or sub[sub_pos].is_discontinuous() or (args.no_singles and len(sub[sub_pos]) <= 1):
                sub = choice(tree_list)
                sub_pos = sub.get_random_label_position(label, True)
        else:
            # print("original tree not discontinuous")
            sub = choice(disco_tree_list)
            sub_pos = sub.get_random_label_position(label, True)
            while sub_pos == None or sub[sub_pos].is_discontinuous() or not sub[sub_pos].has_discontinuity() or (args.no_singles and len(sub[sub_pos]) <= 1):
                sub = choice(disco_tree_list)
                sub_pos = sub.get_random_label_position(label, True)
        

        print(sub)
        print(sub_pos)
        print(sub[sub_pos])
        
        newtree.sub_with_check(tree_pos, sub[sub_pos])
        newtree[tree_pos].set_label(tree[tree_pos].get_label())
        
        if (not newtree in tree_set) and newtree.has_discontinuity():
            print(newtree)
            if args.use_generated_trees:
                tree_list.append(newtree)
                disco_tree_list.append(newtree)
            tree_set.add(newtree)
            aug_tree_list.append(newtree)
            newstr = str(newtree)+"\n"
            str_list.append(newstr)
            i += 1

        print()

if __name__=='__main__':
    # argument parser
    # optional arguments
    parser = argparse.ArgumentParser(description="Takes a discbracket file and returns an augmented version based on substructure substitutions.")
    parser.add_argument('-n', type=int, help='number of new phrases to add to augmented file (takes precedence over percentage if an input is given)')
    parser.add_argument('-p', type=float, help='percentage of new phrases to add to augmented file', default=1)
    label_types = ['NP', 'VP', 'VPinf']
    parser.add_argument('--label', type=str, help="label of structure to be substituted (NP, VP, ...)", default='NP', choices=label_types)
    parser.add_argument('--disco-only', action="store_true", help="generate only discontinuous trees")
    parser.add_argument('--use-generated-trees', action="store_true", help="allow the use of trees generated by the algorithm to produce new trees")
    parser.add_argument('--allow-disco-sub', action="store_true", help="allow the substitution of constituencies with a constituency that is not discontinuous itself but contains a discontinuity in a deeper level")
    parser.add_argument('--no-singles', action="store_true", help="forbids substitutions by phrases with only one token (word)")
    # positional arguments
    parser.add_argument('infile', type=str, help='name of the discbracket file')
    parser.add_argument('outfile', type=str, help='name of the augmented file')

    args = parser.parse_args()

    if args.disco_only:
        print("\nDiscontinuous trees only.\n")

    # file
    infile = open(args.infile, "r")
    print("Reading file {}\n".format(args.infile))

    # string and tree list and set
    str_list = []
    tree_list = []
    tree_set = set()

    # read strings from discbracket file
    # and append to string list
    print("Reading strings from file...")
    line = infile.readline()
    while line != "":
        str_list.append(line)
        line = infile.readline()
    print("Finished reading strings from file.\n")

    # convert strings to DiscoTree
    # and append to tree list
    # and add to tree set
    print("Converting strings to trees...")
    for string in str_list:
        tree = DiscoTree.fromstring(string)
        tree_list.append(tree)
        tree_set.add(tree)
    print("Finished converting to trees.\n")
    old_num_trees = len(tree_list)

    aug_tree_list = copy(tree_list)

    # get list of discontinuous trees
    disco_tree_list = []
    for tree in tree_list:
        if tree.has_discontinuity():
            disco_tree_list.append(tree)
    num_of_discont = len(disco_tree_list)

    label = args.label
    if args.n != None:
        total = args.n
    else:
        total = round(args.p * old_num_trees)

    if not args.disco_only:
        produce_trees()
    else:
        produce_disco_trees()

    new_num_discont = count_discontinous(aug_tree_list)
    new_num_trees = len(aug_tree_list)

    print("\n{} : {} discontinuous trees".format(args.infile, num_of_discont))
    print("{} : {} total trees".format(args.infile, old_num_trees))
    print("Discontinuous percentage : {}% of all trees".format(round(num_of_discont/old_num_trees*100, 2)))
    print("\n{} : {} discontinuous trees".format(args.outfile, new_num_discont))
    print("{} : {} total trees".format(args.outfile, new_num_trees))
    print("Discontinuous percentage : {}% of all trees".format(round(new_num_discont/new_num_trees*100, 2)))

        
    print("\nWriting to file {}\n".format(args.outfile))
    outfile = open(args.outfile, "w")
    outfile.writelines(str_list)

    print("Finished augmenting.")