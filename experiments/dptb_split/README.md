# Experiments : DPTB-split

## Description
These experiments were done using the Discontinuous Penn Treebank (Evang and Kallmeyer, 2011). In this experiment, we aim to see the effect of the augmentation according to the size of the corpus. Here we sample the original DPTB corpus in sizes of 10000, 5000, 2000, 1000, 500, 200, and 100. The larger corpora contain all the examples contained within the smaller corpora (i.e. the n=500 corpus contains what is in the n=200 which contains everything in the n=100, etc.). We also tried two augmentation strategies: one where we add only discontinuous trees (`disco-only`), and one where it is mixed (`mixed`). In total, we did 21 experiments.

### <b>Warning: mtgpy version</b>
This experiment uses an older version of the mtgpy trainer. In this experiment, the model selection takes into account only the global f-score and not the discontinuous f-score. 
This is the hash of the git commit for the version used in this experiment:
`a99c9bec8c2f4eb90a26fe7e8f567adc1f568bdb`

## Augmenting the corpus
In order to take a random sample from the original training corpus, we used the Linux `shuf` command. Here are the command lines:
```sh
sort train.discbracket | shuf -n 10000 > train_10000.discbracket
sort train_10000.discbracket | shuf -n 5000 > train_5000.discbracket
...
sort train_200.discbracket | shuf -n 100 > train_100.discbracket
```

The training corpus was augmented using the option `--no-singles`. Some also used the `--disco-only` The substitutions were done on NP substructures. The augmentation is set at 100% therefore the size is double the original corpus. Here is the command line that can be used to replicate the augmentation:
```sh
# disco-only:
python augment.py --no-singles --disco-only --label NP -p 1 <input-file> <output-file>

# mixed:
python augment.py --no-singles --label NP -p 1 <input-file> <output-file>
```

### Augmentation results
Here are statistics on the amount of discontinuous phrases found in the original and augmented corpora:

![](augment_results.png)

## Running the experiment
<b>Note : if running experiments on a GPU, the option `--gpu 0` is required.</b>

In order to launch the experiment, execute this command line:
```sh
python3 mtg.py train --bert --no-word -c 64 -C 128 --dcu 0.2 -B 32 --diff 0.3 -l 0.00006 <model> <train> <dev>
```

## Results
Each folder contains the logs of the experiments in the `.stderr` and `.stdout` files and the best f-score obtained in tests on the dev corpus during training (`best_dev_score`). Due to size constraints, not all the pretrained models are uploaded.

### Pretrained models
The list of pretrained models that are uploaded can be found in the experiments/models directory.

### Conclusions
With this experiment, we can see the augmentation method works well as intended and there is a significant improvement in the f-scores for small-sized samples (`n=100` and `n=200`). Starting from around `n=2000` the effects of the augmentation no longer gives a significant change to the results of the training. 