# Pretrained models

Due to storage space constraints in the repository, only several pretrained models are released. They can be found in this directory. There are 7 pretrained models uploaded here.

## List of models

### DPTB
We release 2 pretrained models from this experiment.

- `dptb-base-bert` : Base DPTB with no augmentations and trained using BERT
- `dptb-NPaug-p100-bert` : DPTB augmented with 100% generated phrases (doubled) and trained using BERT

### DPTB split
We release 3 pretrained models from this experiment.

- `dptb-200-base-bert` : 200 uniformly sampled phrases from the base DPTB corpus with no augmentations and trained using BERT.
- `dptb-200-mixed-bert` : 400 phrases (200 uniformly sampled from DPTB from the base experiment and 200 from a mixed augmentation) trained using BERT.
- `dptb-200-disco-only-bert` : 400 phrases (200 uniformly sampled from DPTB from the base experiment and 200 from a discontinuous phrases only augmentation) trained using BERT.

### FTB
We release 2 pretrained models from this experiment.

- `ftb-base-bert` : Base FTB with no augmentations and trained using BERT
- `ftb-NPaug-p40-disco-only-bert` : FTB augmented by +40% with only discontinuous phrases and trained using BERT